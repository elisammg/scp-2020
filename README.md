## Softwares de Colaboración y Productividad

Aplicación de sistemas de colaboración y productividad para el manejo de herramientas para la empresa seleccionada.
1. Sistema para manejo de Agile: Azure DevOps
2. Uso de repositorio: Bitbucket
3. Documentación: Confluence
Herramientas a implementar para la colaboración y productividad:
1. ERP
2. CRM
3. PMIS
4. Education Software
5. HRIS
6. Colaboration Platform
7. Workflow Builder
8. CMS
De las herramientas mencionadas se encuentran implementaciones a nivel Enterprise, PyMe y StartUp.